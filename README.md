# backend Internship



## Ejecutar el servidor


Instalar dependencias

```
npm i
```

Instalar globalmente nodemon

```
npm install nodemon --global
````

Ejecutar servidor

```
npm start
```


## Ejercicio

Las peticiones a APIs externas (Rick and Morty) se hacen con [axios](https://axios-http.com/docs/intro) y el servidor esta implementado con [express](https://expressjs.com/en/starter/hello-world.html). Para probar el correcto funcionamiento de las APIs usar [POSTMAN](https://www.postman.com/downloads/).


### Rick and Morty API

Crea un API rest usando la API [Rick and Morty](https://rickandmortyapi.com/documentation) donde regrese una ubicacion dependiendo del **ID** y otra API rest donde regrese el episodio dependiendo del **ID**. Usa el ejemplo de implementacion de personajes que trae un personaje dependiendo el **ID**.

Seguir las buenas practicas de programacion, javascript, API rest, etc.

### Users API

Tomando el ejemplo de creacion de usuario, crear una API rest para actualizar un usuario dependiendo de su **ID** y una API rest para editar un usuario tambien dependiendo de su **ID**. Usa los metodos de arreglos para manipular el arreglo de usuarios y las caracteristicas de ES6 como destructuring para hacer las operaciones. Recuerda la inmutabilidad.


Seguir las buenas practicas de programacion, javascript, API rest, etc.

![](https://i0.wp.com/codigoespagueti.com/wp-content/uploads/2021/04/He-Man-Meme.jpg?resize=640%2C331&quality=80&ssl=1){width='100px'}