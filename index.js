const bodyParser = require("body-parser");
const express = require("express");
const { getCharacter } = require("./operations/getCharacter");
const { createUser } = require("./operations/createUser");

const PORT = 3000;
const BASE_URL_RM = "/enjoy/api/rm/v1";
const BASE_URL_USERS = "/enjoy/api/users/v1";

// Definicion de app y middelwares
const app = express();
app.use(bodyParser.json());

//  Ruta base
app.get("/", (req, res) => res.send("Server running"));

// Rick & Morty API

app.get(`${BASE_URL_RM}/character/:id`, async (req, res) => {

  console.log("ID", req.params?.id)
  console.log(typeof req.params?.id)
  const id = req.params?.id ?? 0;

  try {
    const result = await getCharacter(id) ?? [];
    res.json(result);
  } catch (err) {
    res.json({
      success: false,
      message: `${err}`,
    });
  }
});



// Crear API que consuma la API de Rick y Morty de locations


// Crear API que consuma la API de Rick y Morty de episodes


// USERS API

app.post(`${BASE_URL_USERS}/create`, async (req, res) => {
  try {
    const result = await createUser(req.body);
    res.json({
      success: true,
      message: result,
    });
  } catch (err) {
    res.json({
      success: false,
      message: `${err}`,
    });
  }
});


// Crear API para actualizar usuario dependiendo del ID

// Crear API para eliminar usuario dependiendo del ID

app.listen(PORT, () => console.log(`Server Running ${PORT}!`));
