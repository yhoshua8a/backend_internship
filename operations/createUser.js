let users = [
  {
    id: 1,
    username: "yhoshua8a",
    type: "admin",
    permissions: ["create", "update", "detele"],
    creationDate: "20/12/2023",
  },
];

function createUser(user = {}) {
  return new Promise((resolve, reject) => {
    if (!user.id || !user.username || !user.type || !user.permissions) {
      reject(new Error("All the properties are required to create new user"));
    }

    try {
      users = [
        ...users,
        {
          id: users.length + 1,
          username: user.username,
          type: user.type,
          permissions: user.permissions,
          creationDate: new Date(),
        },
      ];

      console.log("USERS...", users);

      resolve(`the user ${user.username} was created successfully`);
    } catch (err) {
      reject(err);
    }
  });
}

module.exports = {
  createUser: createUser,
};
